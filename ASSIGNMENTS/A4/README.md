# Assignment 4
**Due Oct 1<sup>st</sup> before class**

Start with the handout code and make a word cloud on "I have a dream" speech. Words that occur more number of times should appear larger. Use only words which are 5+ characters.

## Usage

Install ![nodejs](https://nodejs.org)


`` cd A4/handout ``

`` sudo npm install -g http-server ``

`` http-server ``


## Desired output

![UI](img.png "UI")

