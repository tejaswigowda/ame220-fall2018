# Assignment 8
**Due Nov 28<sup>th</sup> before midnight**

Start with `responsiveClockApp` (done in class), and complete the
project as follows:

1. Clean (CSS) + design (5pts)
2. Animations for screen transitions (10 pts)
3. Publish your work on Github pages (5pts).

