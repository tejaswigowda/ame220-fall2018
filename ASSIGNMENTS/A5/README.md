# Assignment 5
**Due Oct 17<sup>st</sup> before class**

Start with the slideshow code (dynamic slide show `$(CLASS_REPO)/dynamicSlideShow/slidingEffect.html`), and implement the same functionality without using JQuery.

## Grading
1. No jQuery used for sliding (use CSS transitions instead).
2. Slide must shake when no movement possible.
3. Improve the look-and-feel (write some CSS).
