# Assignment 7
**Due Nov 14<sup>st</sup> before midnight**

Start with `rssReaderApp` (done in class), and complete the project.

You should make sure your submission is:
1. Clean (CSS) on Desktop (5pts)
2. Responsive (10pts)
3. Design (5pts).

## Usage

### Run
`$ node server.js`

### Test
[http://127.0.0.1:1234](http://127.0.0.1:1234)
