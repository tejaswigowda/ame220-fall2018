# Assignment 3
**Due Sep 24<sup>th</sup> before class**

Start with the handout code and make the slideshow work using jQuery. Unlink the example in class, we only have one <img> tag, whose `src` attribute you will need to change.
