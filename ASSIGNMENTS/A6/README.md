# Assignment 6
**Due Oct 31<sup>st</sup> before midnight**

Start with handout and implement a world clock. You need to use the embeddable `clock.html` (in handout; identical to the clock we worked on in class). Also look at `multiWorldClockEg.html` fo inspiration.
